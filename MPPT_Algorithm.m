%% MPPT Algorithm, as given by Naki GÜLER in his Video
function duty = MPPT_Algorithm(V_PV, I_PV, delta)
    % First, define the initial Duty Ratio (D)
    D_INIT = 0.05;
    % A min & max D is defined
    D_MIN = 0;
    D_MAX = 0.75;
    
    persistent V_OLD P_OLD D_OLD; % defined, so that these variables,
    % can be accessed everytime, and the old value can be obtained by the
    % difference the old and the new value.
    if isempty(V_OLD)
        V_OLD = 0;
        P_OLD = 0;
        D_OLD = D_INIT;
    end
    
    P = V_PV * I_PV; % Calculated Power
    
    % Calculating delta values, for MPPT Algorithm
    dV = V_PV - V_OLD;
    dP = P - P_OLD;
    
    %% P&O Method of MPPT Tracking Algorithm
    if dP ~= 0 && V_PV > 30
        if dP < 0
            if dV < 0
                duty = D_OLD - delta;
            else
                duty = D_OLD + delta;
            end
        else
            if dV < 0
                duty = D_OLD + delta;
            else
                duty = D_OLD - delta;
            end
        end
    else
        duty = D_OLD;
    end
    
    if duty >= D_MAX
        duty = D_MAX;
    elseif duty < D_MIN
        duty = D_MIN;
    end
    
    %% Setting the OLD Values with the Current Condition Value
    D_OLD = duty;
    V_OLD = V_PV;
    P_OLD = P;