# Simulation and Design of Power Converters

<b>Connect with me at:</b>
<a href = "https://www.linkedin.com/in/dpramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/linkedin.svg"/></a>
	<a href = "https://github.com/ZenithClown"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/github.svg"/></a>
	<a href = "https://gitlab.com/ZenithClown/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/gitlab.svg"/></a>
	<a href = "https://www.researchgate.net/profile/Debmalya_Pramanik2"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/researchgate.svg"/></a>
	<a href = "https://www.kaggle.com/dPramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/kaggle.svg"/></a>
	<a href = "https://app.pluralsight.com/profile/Debmalya-Pramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/pluralsight.svg"/></a>
	<a href = "https://stackoverflow.com/users/6623589/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/stackoverflow.svg"/></a>

Simulation is done in MatLAB - Simulink R2020a
For more nnformation and help on the design, please check [Naki GÜLER's YouTube Lecture on this Topic](https://www.youtube.com/watch?v=jPtTNRGBO8E&list=PL45pBmqeuIrfVXmUrUZz7b0Sm4yqTl6a2).

## Integration of PV Array with Battery
A [PV Module 1STH-215-P](http://www.solarhub.com/product-catalog/pv-modules/5623-1STH-215-P-1Soltech) is used in this module, and a system of 1 kW is designed, for this purpose the array of 5-strings with each string containing a single module is used.