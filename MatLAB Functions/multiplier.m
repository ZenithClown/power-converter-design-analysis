function amplitude = GAIN(current_amplitude, multiplier)
    amplitude = current_amplitude * multiplier;